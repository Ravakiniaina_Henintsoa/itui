/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import entity.Question;
import entity.Questionreponse;
import entity.Reponse;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import managedBean.util.Util;
import session.QuestionReponseManager;
import session.QuestionManager;
import session.ReponseManager;

/**
 *
 * @author pro
 */
@Named(value = "questionReponse")
@ViewScoped

public class QuestionReponseMBean implements Serializable{

    @EJB
    private QuestionReponseManager questionReponseManager;
    @EJB
    private QuestionManager questionManager;
    @EJB
    private ReponseManager reponseManager;
    
    private final Questionreponse qr = new Questionreponse();
    private Questionreponse questr;
    private Long id;

    public Questionreponse getQuestr() {
        return questr;
    }
    public Questionreponse get_questionreponse() {
        return qr;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public List<Questionreponse> getAllQuestionreponse() {
        return questionReponseManager.getAllQuestionreponse();
    }
    
    public List<Question> getAllQuestion() {
        return questionManager.getAllQuestion();
    }
    
    public List<Reponse> getAllReponse() {
        return reponseManager.getAllReponse();
    }

    public String supprimerQuestionreponse(Questionreponse questionreponse) {
        questionReponseManager.supprimerQuestionreponse(questionreponse);
        Util.addFlashInfoMessage("Question reponse " + questionreponse.getId() + " supprimé");
        return "liste?faces-redirect=true";
    }
    
    public String enregistrer() {
        questionReponseManager.update(questr);
        return "liste?faces-redirect=true";
    }
    
    public String creerQuestionreponse() {
        questionReponseManager.creerQuestionReponse(qr);
        return "liste?faces-redirect=true";
    }
    
}
