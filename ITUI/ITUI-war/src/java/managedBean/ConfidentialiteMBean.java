/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import entity.Confidentialite;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import session.ConfidentialiteManager;
import managedBean.util.Util;

/**
 *
 * @author pro
 */
@Named(value = "confidentialite")
@ViewScoped

public class ConfidentialiteMBean implements Serializable{

    @EJB
    private ConfidentialiteManager confidentialiteManager;
    private final Confidentialite c = new Confidentialite();
    private Confidentialite conf;
    private Long id;

    public Confidentialite getConf() {
        return conf;
    }
    public Confidentialite get_confidentialite() {
        return c;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public List<Confidentialite> getAllConfidentialite() {
        return confidentialiteManager.getAllConfidentialite();
    }

    public String supprimerConfidentialite(Confidentialite confidentialite) {
        confidentialiteManager.supprimerConfidentialite(confidentialite);
        Util.addFlashInfoMessage("Confidentialite " + confidentialite.getLibelle() + " supprimé");
        return "liste?faces-redirect=true";
    }
    
    public Confidentialite getConfidentialiteId(Confidentialite confidentialite) {
        conf = confidentialiteManager.getConfidentialite(confidentialite.getId());
        System.out.println("-------------"+conf);
        return conf;
    }
    
    public String enregistrer() {
        confidentialiteManager.update(conf);
        return "liste?faces-redirect=true";
    }
    
    public String creerConfidentialite() {
        confidentialiteManager.creerConfidentialite(c);
        return "liste?faces-redirect=true";
    }
    
}
