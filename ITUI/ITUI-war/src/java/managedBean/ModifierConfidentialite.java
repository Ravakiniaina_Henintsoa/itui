package managedBean;


import entity.Confidentialite;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import session.ConfidentialiteManager;

/**
 * Backing bean pour la page modifierNom.xhtml.
 *
 * @author pRo
 */

@Named(value = "modifierConfidentialite")
@ViewScoped

public class ModifierConfidentialite implements Serializable {
    @EJB
    private ConfidentialiteManager confidentialiteManager;
    
    private Confidentialite conf;
    private Long id;

    public Confidentialite getConf() {
        return conf;
    }
   
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getConfidentialiteId(Confidentialite confidentialite) {
        conf = confidentialiteManager.getConfidentialite(confidentialite.getId());
        return "modif?faces-redirect=true";
    }
    
    public String enregistrer() {
        confidentialiteManager.update(conf);
        return "liste?faces-redirect=true";
    }
    
}
