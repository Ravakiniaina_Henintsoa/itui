/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import entity.Confidentialite;
import entity.Question;
import entity.Reponse;
import entity.Utilisateur;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.faces.convert.Converter;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import managedBean.util.Util;
import session.ConfidentialiteManager;
import session.QuestionManager;
import session.UtilisateurManager;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import session.ReponseManager;



/**
 *
 * @author pro
 */

@Named(value = "reponse")
@ViewScoped

public class ReponseMBean implements Serializable{
    
    @EJB
    private ReponseManager reponseManager;
    
    @EJB
    private UtilisateurManager utilisateurManager;
    
    @EJB
    private ConfidentialiteManager confidentialiteManager;
    
    private final Reponse r = new Reponse();
    
    public Reponse get_reponse() {
        return r;
    }
    
    public List<Reponse> getAllReponse() {
        return reponseManager.getAllReponse();
    }
    
    public List<Utilisateur> getAllUtilisateur() {
      return utilisateurManager.getAllUtilisateur();
    }
    
    public List<Confidentialite> getAllConfidentialite() {
      return confidentialiteManager.getAllConfidentialite();
    }
    
    public String supprimerReponse(Reponse reponse) {
        reponseManager.supprimerReponse(reponse);
        Util.addFlashInfoMessage("Reponse " + reponse.getLibelle() + " supprimé");
        return "liste?faces-redirect=true";
    }
    
    public String creerReponse() {
        reponseManager.creerReponse(r);
        return "liste?faces-redirect=true";
    }
   
}
