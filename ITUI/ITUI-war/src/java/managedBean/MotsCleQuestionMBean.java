/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import entity.*;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import managedBean.util.Util;
import session.MotsCleManager;
import session.MotsCleQuestionManager;
import session.QuestionManager;

/**
 *
 * @author pro
 */
@Named(value = "motsCleQuestion")
@ViewScoped

public class MotsCleQuestionMBean implements Serializable{

    @EJB
    private MotsCleQuestionManager motsCleQuestionManager;
    @EJB
    private MotsCleManager motsCleManager;
    @EJB
    private QuestionManager questionManager;
    
    private final Motsclequestion m = new Motsclequestion();
    private Motsclequestion motsCleQuestion;
    private Long id;
    
    public Motsclequestion getMotsCleQuestion() {
        return motsCleQuestion;
    }
    public Motsclequestion get_motsCleQuestion() {
        return m;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public List<Motsclequestion> getAllMotsCleQuestion() {
        return motsCleQuestionManager.getAllMotsCleQuestion();
    }
    
    public List<Motscle> getAllMotsCle() {
        return motsCleManager.getAllMotsCle();
    }
    
    public List<Question> getAllQuestion() {
        return questionManager.getAllQuestion();
    }

    public String supprimerMotsCleQuestion(Motsclequestion motsCleQuestion) {
        motsCleQuestionManager.supprimerMotsCleQuestion(motsCleQuestion);
        Util.addFlashInfoMessage("Mots Cle Question " + motsCleQuestion.getMotscleId().getContenu() + " supprimé");
        return "liste?faces-redirect=true";
    }
    
    public String getMotsCleQuestionId(Motsclequestion motsCleQuestion) {
        motsCleQuestion = motsCleQuestionManager.getMotsCleQuestion(motsCleQuestion.getId());
        return "modif?faces-redirect=true";
    }
    
    public String enregistrer() {
        motsCleQuestionManager.update(motsCleQuestion);
        return "liste?faces-redirect=true";
    }
    
    public String creerMotsCleQuestion() {
        motsCleQuestionManager.creerMotsCleQuestion(m);
        return "liste?faces-redirect=true";
    }
    
}
