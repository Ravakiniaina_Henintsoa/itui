/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import entity.Role;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import managedBean.util.Util;
import session.RoleManager;

/**
 *
 * @author pro
 */
@Named(value = "role")
@ViewScoped

public class RoleMBean implements Serializable{

    @EJB
    private RoleManager roleManager;
    private final Role r = new Role();
    private Role role;
    private Long id;
    
    public Role getRole() {
        return role;
    }
    public Role get_role() {
        return r;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public List<Role> getAllRole() {
        return roleManager.getAllRole();
    }

    public String supprimerRole(Role role) {
        roleManager.supprimerRole(role);
        Util.addFlashInfoMessage("Role " + role.getLibelle() + " supprimé");
        return "liste?faces-redirect=true";
    }
    
    public String getRoleId(Role role) {
        role = roleManager.getRole(role.getId());
        return "modif?faces-redirect=true";
    }
    
    public String enregistrer() {
        roleManager.update(role);
        return "liste?faces-redirect=true";
    }
    
    public String creerRole() {
        roleManager.creerRole(r);
        return "liste?faces-redirect=true";
    }
}
