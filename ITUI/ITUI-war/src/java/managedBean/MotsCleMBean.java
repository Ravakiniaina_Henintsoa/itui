/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import entity.Motscle;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import managedBean.util.Util;
import session.MotsCleManager;

/**
 *
 * @author pro
 */
@Named(value = "motsCle")
@ViewScoped

public class MotsCleMBean implements Serializable{
    
    @EJB
    private MotsCleManager motsCleManager;
    private final Motscle m = new Motscle();
    private Motscle mc;
    private Long id;

    public Motscle getMc() {
        return mc;
    }
    public Motscle get_motsCle() {
        return m;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public List<Motscle> getAllMotsCle() {
        return motsCleManager.getAllMotsCle();
    }

    public String supprimerMotsCle(Motscle motsCle) {
        motsCleManager.supprimerMotsCle(motsCle);
        Util.addFlashInfoMessage("Mots Cle " + motsCle.getContenu() + " supprimé");
        return "liste?faces-redirect=true";
    }
    
    public String getMotsCleId(Motscle motsCle) {
        mc = motsCleManager.getMotsCle(motsCle.getId());
        return "modif?faces-redirect=true";
    }
    
    public String enregistrer() {
        motsCleManager.update(mc);
        return "liste?faces-redirect=true";
    }
    
    public String creerMotsCle() {
        motsCleManager.creerMotsCle(m);
        return "liste?faces-redirect=true";
    }
}
