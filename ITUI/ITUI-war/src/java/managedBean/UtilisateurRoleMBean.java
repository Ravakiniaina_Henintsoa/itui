/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import entity.*;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import managedBean.util.Util;
import session.*;
import session.RoleManager;
import session.UtilisateurManager;
import session.UtilisateurRoleManager;

/**
 *
 * @author pro
 */
@Named(value = "utilisateurrole")
@ViewScoped

public class UtilisateurRoleMBean implements Serializable{

    @EJB
    private UtilisateurRoleManager utilisateurRoleManager;
    @EJB
    private UtilisateurManager utilisateurManager;
    @EJB
    private RoleManager roleManager;
    
    private final Roleutilisateur ur = new Roleutilisateur();
    private Roleutilisateur userR;
    private Long id;

    public Roleutilisateur getUserR() {
        return userR;
    }
    public Roleutilisateur get_utilisateurRole() {
        return ur;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public List<Roleutilisateur> getAllUtilisateurRole() {
        return utilisateurRoleManager.getAllUtilisateurRole();
    }
    
    public List<Utilisateur> getAllUtilisateur() {
        return utilisateurManager.getAllUtilisateur();
    }
    
    public List<Role> getAllRole() {
        return roleManager.getAllRole();
    }

    public String supprimerUtilisateurRole(Roleutilisateur utilisateurRole) {
        utilisateurRoleManager.supprimerUtilisateurRole(utilisateurRole);
        Util.addFlashInfoMessage("Utilisateur role " + utilisateurRole.getId() + " supprimé");
        return "liste?faces-redirect=true";
    }
    
    public String enregistrer() {
        utilisateurRoleManager.update(userR);
        return "liste?faces-redirect=true";
    }
    
    public String creerUtilisateurRole() {
        ur.setMail(ur.getUtilisateur().getMail());
       utilisateurRoleManager.creerUtilisateurRole(ur);
       return "liste?faces-redirect=true";
    }
    
}
