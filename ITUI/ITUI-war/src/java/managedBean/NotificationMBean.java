/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import entity.Confidentialite;
import entity.Notification;
import entity.Question;
import entity.Utilisateur;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import session.ConfidentialiteManager;
import managedBean.util.Util;
import session.NotificationManager;
import session.QuestionManager;
import session.UtilisateurManager;

/**
 *
 * @author pro
 */
@Named(value = "notification")
@ViewScoped

public class NotificationMBean implements Serializable{

    @EJB
    private NotificationManager notificationManager;
    
    @EJB
    private QuestionManager questionManager;
    
    private final Notification n = new Notification();
    private Notification notif;
    private Long id;

    public Notification getNotif() {
        return notif;
    }
    public Notification get_notification() {
        return n;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public List<Notification> getAllNotification() {
        return notificationManager.getAllNotification();
    }
    
    public List<Question> getAllQuestion() {
      return questionManager.getAllQuestion();
    }

    public String supprimerNotification(Notification notification) {
        notificationManager.supprimerNotification(notification);
        Util.addFlashInfoMessage("Notification " + notification.getId() + " supprimé");
        return "liste?faces-redirect=true";
    }
    
    public String enregistrer() {
        notificationManager.update(notif);
        return "liste?faces-redirect=true";
    }
    
    public String creerNotification() {
        notificationManager.creerNotification(n);
        return "liste?faces-redirect=true";
    }
    
}
