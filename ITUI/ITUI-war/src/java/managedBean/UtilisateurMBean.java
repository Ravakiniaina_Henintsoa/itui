/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import entity.Confidentialite;
import entity.Role;
import entity.Utilisateur;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import session.ConfidentialiteManager;
import managedBean.util.Util;
import session.UtilisateurManager;
import session.RoleManager;
import util.HashMdp;

/**
 *
 * @author pro
 */
@Named(value = "utilisateur")
@ViewScoped

public class UtilisateurMBean implements Serializable{

    @EJB
    private UtilisateurManager utilisateurManager;
    
    @EJB
    private RoleManager roleManager;
    
    @Inject
    private HashMdp passwordHash;
    
    private final Utilisateur u = new Utilisateur();
    private Utilisateur user;
    private Long id;

    public Utilisateur getUser() {
        return user;
    }
    public Utilisateur get_utilisateur() {
        return u;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public List<Utilisateur> getAllUtilisateur() {
        return utilisateurManager.getAllUtilisateur();
    }
   
    public List<Role> getAllRole() {
        return roleManager.getAllRole();
    }

    public String supprimerUtilisateur(Utilisateur utilisateur) {
        utilisateurManager.supprimerUtilisateur(utilisateur);
        Util.addFlashInfoMessage("Utilisateur " + utilisateur.getNom() + " supprimé");
        return "liste?faces-redirect=true";
    }
    
    public String enregistrer() {
        utilisateurManager.update(user);
        return "liste?faces-redirect=true";
    }
    
    public String creerUtilisateur() {
        String hashMdp = passwordHash.generate(u.getMdp());
        u.setMdp(hashMdp);
        utilisateurManager.creerUtilisateur(u);
        return "liste?faces-redirect=true";
    }
    
}
