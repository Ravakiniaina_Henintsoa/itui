/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import entity.Confidentialite;
import entity.Question;
import entity.Utilisateur;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.faces.convert.Converter;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import managedBean.util.Util;
import session.ConfidentialiteManager;
import session.QuestionManager;
import session.UtilisateurManager;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;



/**
 *
 * @author pro
 */

@Named(value = "question")
@ViewScoped

public class QuestionMBean implements Serializable{
    
    @EJB
    private QuestionManager questionManager;
    
    @EJB
    private UtilisateurManager utilisateurManager;
    
    @EJB
    private ConfidentialiteManager confidentialiteManager;
    
    private final Question q = new Question();
    
    public Question get_question() {
        return q;
    }
    
    public List<Question> getAllQuestion() {
        return questionManager.getAllQuestion();
    }
    
    public List<Utilisateur> getAllUtilisateur() {
      return utilisateurManager.getAllUtilisateur();
    }
    
    public List<Confidentialite> getAllConfidentialite() {
      return confidentialiteManager.getAllConfidentialite();
    }
    
    public String supprimerQuestion(Question question) {
        questionManager.supprimerQuestion(question);
        Util.addFlashInfoMessage("Question " + question.getLibelle() + " supprimé");
        return "liste?faces-redirect=true";
    }
    
    public String creerQuestion() {
        questionManager.creerQuestion(q);
        return "liste?faces-redirect=true";
    }
   
}
