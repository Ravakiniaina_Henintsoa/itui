

package ejb.session;

import entity.Confidentialite;
import entity.Motscle;
import entity.Question;
import entity.Role;
import entity.Roleutilisateur;
import entity.Utilisateur;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.sql.DataSource;
import session.ConfidentialiteManager;
import session.MotsCleManager;
import session.MotsCleQuestionManager;
import session.NotificationManager;
import session.QuestionManager;
import session.QuestionReponseManager;
import session.ReponseManager;
import session.RoleManager;
import session.UtilisateurManager;
import session.UtilisateurRoleManager;
import util.HashMdp;


/**
 * Initialise les données pour les utilisateurs : création des tables et
 * insertion des données.
 */
@Singleton
@Startup
@DataSourceDefinition(
        className = "org.apache.derby.jdbc.ClientDataSource",
        name = "java:app/jdbc/itui",
        serverName = "localhost",
        portNumber = 1527,
        user = "itui", // nom et
        password = "itui", // mot de passe que vous avez donnés lors de la création de la base de données
        databaseName = "itui"
)
public class Init {

    @Resource(lookup = "java:app/jdbc/itui")
    private DataSource dataSource;
    @Inject
    private HashMdp passwordHash;

    @EJB
    private QuestionManager questionManager;
    @EJB
    private ConfidentialiteManager confidentialiteManager;
    @EJB
    private MotsCleManager motsCleManager;
    @EJB
    private MotsCleQuestionManager motsCleQuestionManager;
    @EJB
    private NotificationManager notificationManager;
    @EJB
    private QuestionReponseManager questionReponseManager;
    @EJB
    private ReponseManager reponseManager;
    @EJB
    private RoleManager roleManager;
    @EJB
    private UtilisateurManager utilisateurManager;
    @EJB
    private UtilisateurRoleManager utilisateurRoleManager;
    
    
    @PostConstruct
    public void init() {
        try (Connection c = dataSource.getConnection()) {
            
            if (roleManager.nbRole() == 0) {
                roleManager.creerRole(new Role("admin"));
                roleManager.creerRole(new Role("utilisateur"));
            }
            if (confidentialiteManager.nbConfidentialite() == 0) {
                confidentialiteManager.creerConfidentialite(new Confidentialite("Public"));
                confidentialiteManager.creerConfidentialite(new Confidentialite("Prive"));
                confidentialiteManager.creerConfidentialite(new Confidentialite("Moi uniquement"));
            }
            if (motsCleManager.nbMotsCle() == 0) {
                motsCleManager.creerMotsCle(new Motscle("Java", 0));
                motsCleManager.creerMotsCle(new Motscle("Oracle", 0));
                motsCleManager.creerMotsCle(new Motscle("Postgres", 0));
            }
            if (utilisateurManager.nbUtilisateur() == 0) {
                String hashMdp = passwordHash.generate("toto");
                utilisateurManager.creerUtilisateur(new Utilisateur("Rakotoharison", "Nambinina", "nambinina@gmail.com", "nambinina", hashMdp, 0));
                Role role = roleManager.getRole(new Long(1));
                Utilisateur utilisateur = utilisateurManager.getUtilisateur(new Long(9));
                utilisateurRoleManager.creerUtilisateurRole(new Roleutilisateur(role, utilisateur, utilisateur.getMail()));
            }
            if (questionManager.nbQuestion() == 0) {
                Confidentialite confidentialite = confidentialiteManager.getConfidentialite(new Long(3));
                Utilisateur utilisateur         = utilisateurManager.getUtilisateur(new Long(9));
                String s                        = "2019-05-15";
                Date date                       = Date.valueOf(s);
                questionManager.creerQuestion(new Question(utilisateur, "Variable", "Developpement", date, "10:00:00", confidentialite, 0, "Java", "int a = 0;"));
            }
            /*if (notificationManager.nbNotification() == 0) {
                Question question = questionManager.getQuestion(new Long(10));
                notificationManager.creerNotification(new Notification(question, 0));
            }
            if (reponseManager.nbReponse() == 0) {
                Question question = questionManager.getQuestion(new Long(10));
                Confidentialite confidentialite = confidentialiteManager.getConfidentialite(new Long(3));
                Utilisateur utilisateur         = utilisateurManager.getUtilisateur(new Long(9));
                String s                        = "2019-05-15";
                Date date                       = Date.valueOf(s);
                reponseManager.creerReponse(new Reponse(utilisateur, "Execution des programmes compiles", confidentialite, date, "10:00:00", 2, 0, question));
            }
            if (motsCleQuestionManager.nbMotsCleQuestion() == 0) {
                Question question   = questionManager.getQuestion(new Long(10));
                MotsCle motsCle     = motsCleManager.getMotsCle(new Long(6));
                motsCleQuestionManager.creerMotsCleQuestion(new MotsCleQuestion(question, motsCle));
            }
            if (questionReponseManager.nbQuestionreponse() == 0) {
                Reponse reponse   = reponseManager.getReponse(new Long(12));
                Question question   = questionManager.getQuestion(new Long(10));
                questionReponseManager.creerQuestionReponse(new Questionreponse(question, reponse));
            }*/
            
        } catch (SQLException e) {
            // Une méthode annotée avec @PostConstruct ne peut lancer d'exception contrôlée.
            e.printStackTrace();
        }
    }

    private void execute(Connection c, String query) {
        try (PreparedStatement stmt = c.prepareStatement(query)) {
          stmt.executeUpdate();
        } catch (SQLException e) {
          // Pour les logs du serveur d'application
          e.printStackTrace();
        }
    }

  /**
   * Teste si une table existe déjà.
   *
   * @param connection
   * @param nomTable nom de la table ; attention la casse compte !
   * @return true ssi la table existe.
   * @throws SQLException
   */
    private static boolean existe(Connection connection, String nomTable)
            throws SQLException {
        boolean existe;
        DatabaseMetaData dmd = connection.getMetaData();
        try (ResultSet tables = dmd.getTables(connection.getCatalog(), null, nomTable, null)) {
          existe = tables.next();
        }
        return existe;
    }

  /**
   *
   * @return true ssi la table LOGIN est vide.
   */
    private boolean vide(Connection c, String nomTable) throws SQLException {
        Statement stmt = c.createStatement();
        ResultSet rset = stmt.executeQuery("select count(1) from " + nomTable);
        rset.next();
        int nb = rset.getInt(1);
        return nb == 0;
    }
}

 
