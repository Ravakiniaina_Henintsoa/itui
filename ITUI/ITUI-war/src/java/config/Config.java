package config;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.annotation.FacesConfig;
import javax.security.enterprise.authentication.mechanism.http.CustomFormAuthenticationMechanismDefinition;
import javax.security.enterprise.authentication.mechanism.http.LoginToContinue;
import javax.security.enterprise.identitystore.DatabaseIdentityStoreDefinition;

/**
 * Configuration de l'application pour JSF 2.3 et pour la sécurité.
 */
@ApplicationScoped
@FacesConfig
@DatabaseIdentityStoreDefinition(
  dataSourceLookup = "jdbc/itui",
  callerQuery = "select mdp from utilisateur where login=?",
  groupsQuery = "select role from utilisateur where login=?"
)
@CustomFormAuthenticationMechanismDefinition(
  loginToContinue = @LoginToContinue(
    loginPage = "/Login.xhtml",
    errorPage = ""
  )
)
public class Config {
  
}