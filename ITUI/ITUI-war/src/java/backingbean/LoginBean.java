/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backingbean;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.security.enterprise.AuthenticationStatus;
import static javax.security.enterprise.AuthenticationStatus.SEND_CONTINUE;
import static javax.security.enterprise.AuthenticationStatus.SEND_FAILURE;
import javax.security.enterprise.SecurityContext;
import static javax.security.enterprise.authentication.mechanism.http.AuthenticationParameters.withParams;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.credential.Password;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Backing bean pour la page de login avec un formulaire personnalisé (custom).
 *
 * @author Mirado
 */
@Named(value = "loginBean")
@RequestScoped
public class LoginBean {

    @Inject
    private SecurityContext securityContext;
    @Inject
    private FacesContext facesContext;
    @Inject
    private ExternalContext externalContext;

    private String nom;
    private String motDePasse;

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void login() {
        Credential credential
                = new UsernamePasswordCredential(nom, new Password(motDePasse));
        AuthenticationStatus status = securityContext.authenticate(
                (HttpServletRequest) externalContext.getRequest(),
                (HttpServletResponse) externalContext.getResponse(),
                withParams().credential(credential));
        if (status.equals(SEND_CONTINUE)) {
            facesContext.responseComplete();
        } else if (status.equals(SEND_FAILURE)) {
            addError(facesContext, "Nom / mot de passe incorrects");
        }
    }

    /**
     * Ajoute une erreur à afficher dans la page de login.
     *
     * @param facesContext
     * @param authentication_failed
     */
    private void addError(FacesContext facesContext,
            String message) {
        facesContext.addMessage(
                null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        message,
                        null));
    }

    public String logout() {
        try {
            HttpServletRequest httpServletRequest = (HttpServletRequest) externalContext.getRequest();
            httpServletRequest.logout();
            httpServletRequest.getSession().invalidate();
        } catch (ServletException ex) {
            System.err.println("Erreur pendant logout : " + ex);
        }
        return "liste?faces-redirect=true";
    }
}
