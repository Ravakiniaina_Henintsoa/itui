/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

import entity.Role;
import managedBean.*;
import entity.Utilisateur;
import java.io.Serializable;
import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import session.RoleManager;


/**
 *
 * @author pro
 */

@Named("RoleConverter")
@RequestScoped
public class RoleConverter  implements Converter, Serializable{

    @EJB
    private RoleManager roleManager;
    
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        //System.out.println("----->"+value);
        Role role= roleManager.getRole(Long.parseLong(value));
        return role;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        Long id = (o instanceof Role) ? ((Role) o).getId() : null;
        //System.out.println("------------ici = "+id);
        return (id != null) ? String.valueOf(id) : null;
    }

}


