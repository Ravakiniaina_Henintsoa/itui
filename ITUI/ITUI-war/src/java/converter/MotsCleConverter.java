/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

import entity.Motscle;
import entity.Question;
import managedBean.*;
import entity.Utilisateur;
import java.io.Serializable;
import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import session.ConfidentialiteManager;
import session.MotsCleManager;
import session.QuestionManager;
import session.UtilisateurManager;


/**
 *
 * @author pro
 */

@Named("MotsCleConverter")
@RequestScoped

public class MotsCleConverter  implements Converter, Serializable{

    @EJB
    private MotsCleManager motsCleManager;
    
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        //System.out.println("----->"+value);
        Motscle motsCle= motsCleManager.getMotsCle(Long.parseLong(value));
        return motsCle;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        Long id = (o instanceof Motscle) ? ((Motscle) o).getId() : null;
        //System.out.println("------------ici = "+id);
        return (id != null) ? String.valueOf(id) : null;
    }

}


