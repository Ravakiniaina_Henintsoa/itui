/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

import entity.Confidentialite;
import managedBean.*;
import java.io.Serializable;
import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import session.ConfidentialiteManager;


/**
 *
 * @author pro
 */

@Named("ConfidentialiteConverter")
@RequestScoped
public class ConfidentialiteConverter  implements Converter, Serializable{

    @EJB
    private ConfidentialiteManager confidentialiteManager;
    
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        //System.out.println("----->"+value);
        Confidentialite confidentialite= confidentialiteManager.getConfidentialite(Long.parseLong(value));
        return confidentialite;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        Long id = (o instanceof Confidentialite) ? ((Confidentialite) o).getId() : null;
        //System.out.println("------------ici = "+id);
        return (id != null) ? String.valueOf(id) : null;
    }

}


