/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbean;

import entity.Utilisateur;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import session.UtilisateurManager;

/**
 *
 * @author Nante-PC
 */
@Named(value = "profil")
@ViewScoped
public class UtilisateurBean implements Serializable {
    Utilisateur utilisateur;
    private String mail;
    private String mdp;
    private String msg;

    @EJB
    private UtilisateurManager userManager;
    public Utilisateur getUser() {
        utilisateur = userManager.getUtilisateur(Long.valueOf(10));
        return utilisateur;
    }
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
    

    public void setMsg(String msg) {
        this.msg = msg;
    }
    public String getMsg() {
        return msg;
    }
    
    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }
    public void modifier(Long id){
        Utilisateur user = userManager.getUtilisateur(id);
        user.setMail(this.mail);
        user.setMdp(this.mdp);
        userManager.update(user);
        this.setMsg("Vos informations ont été mis à jour avec succès");
    }
}