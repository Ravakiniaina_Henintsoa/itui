/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbean;

import entity.Reponse;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import session.ReponseManager;

/**
 *
 * @author Nante-PC
 */
@Named(value = "reponseBean")
@ViewScoped
public class ReponseBean implements Serializable {
    private List < Reponse > responseList;
    /**
     * Creates a new instance of ReponseBean
     */
    public ReponseBean() {}
    @EJB
    private ReponseManager reponseManager;
    public List < Reponse > getReponse() {
        responseList = reponseManager.getAllReponse();
        return responseList;
    }
}