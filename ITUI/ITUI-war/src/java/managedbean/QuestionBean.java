/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbean;

import entity.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;
import session.ConfidentialiteManager;
import session.QuestionManager;
import session.UtilisateurManager;
/**
 *
 * @author Nante-PC
 */
@Named(value = "questionBean")
@ViewScoped
public class QuestionBean implements Serializable {
    private List < Question > QuestionList;
    private int id;
    private String msg;
    private Question q;
    private String categorie;
    private String libelle;

    @EJB
    private QuestionManager questionManager;
    private UtilisateurManager userManager;
    private ConfidentialiteManager confManager;
    
    public QuestionBean() {}
    public List <Question> getQuestion() {
        QuestionList = questionManager.getAllQuestion();
        return QuestionList;
    }

    public void Supprimer(Long id) {
        Question q = questionManager.getQuestion(id);
        q.setEtat(1);
        questionManager.update(q);
    }

    public String Voir(Long id) {
        return "detailsQuestion?faces-redirect=true&id="+id;
    }

    public String voirReponse(Long id) {
        return "listeReponse?faces-redirect=true&id=" + id;
    }
    public Question detailsQuestion() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        this.id = Integer.parseInt(request.getParameter("id"));
        q = questionManager.getQuestion(Long.valueOf(id));
        return q;
    }
    
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public String getMsg() {
        return msg;
    }
    
    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }
    public String getCategorie() {
        return categorie;
    }
    
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    public String getLibelle() {
        return libelle;
    }
    
    public void Modifier(Long id) {
        Question question = questionManager.getQuestion(id);
        question.setCategorie(this.q.getCategorie());
        question.setLibelle(this.q.getLibelle());
    }
    
    public void ajouter(){
        //Date date = new Date();
        //Calendar cal = Calendar.getInstance();
        //String heure = cal.get(Calendar.HOUR_OF_DAY)+"h "+cal.get(Calendar.MINUTE)+"m et "+cal.get(Calendar.SECOND)+"s";
        //Long id = questionManager.nbQuestion();
        //Confidentialite conf = confManager.getConfidentialite(1);
    }
}