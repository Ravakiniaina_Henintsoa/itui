/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Utilisateur;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Nante-PC
 */
@Stateless
@LocalBean
public class UtilisateurManager {
    @Resource
    private SessionContext sessionContext;
    
    @PersistenceContext(unitName = "ITUI-ejbPU")
    private EntityManager em;

    public void creerUtilisateur(Utilisateur utilisateur) {
        em.persist(utilisateur);
    }
    
    public void supprimerUtilisateur(Utilisateur utilisateur) {
        em.remove(em.merge(utilisateur));
    }

    public List<Utilisateur> getAllUtilisateur() {
        String requete = "select c from Utilisateur c";
        TypedQuery<Utilisateur> query = em.createQuery(requete, Utilisateur.class);
        return query.getResultList();
    }
    
    public Utilisateur findByNom(String nom) {
        System.out.print("Anaty fonction");
        TypedQuery<Utilisateur> query = em.createQuery("Utilisateur.findByLogin", Utilisateur.class);
        System.out.print("nom"+nom);
        query.setParameter("login", nom);
        return query.getSingleResult();
    }
    
    public Utilisateur getUtilisateur(Long id) {
        return em.find(Utilisateur.class, id);
    }
    public Utilisateur update(Utilisateur utilisateur) {
        return em.merge(utilisateur);
    }

    public long nbUtilisateur() {
        String requete = "select count(c) from Utilisateur c";
        TypedQuery<Long> query = em.createQuery(requete, Long.class);
        return query.getSingleResult();
    }
        
}
