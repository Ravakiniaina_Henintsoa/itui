/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Role;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Nante-PC
 */
@Stateless
@LocalBean
public class RoleManager {
    @Resource
    private SessionContext sessionContext;
    
    @PersistenceContext(unitName = "ITUI-ejbPU")
    private EntityManager em;

    public void creerRole(Role role) {
        em.persist(role);
    }

    public List<Role> getAllRole() {
        String requete = "select c from Role c";
        TypedQuery<Role> query = em.createQuery(requete, Role.class);
        return query.getResultList();
    }

    public void supprimerRole(Role role) {
        em.remove(em.merge(role));
    }

    public Role getRole(long id) {
        return em.find(Role.class, id);
    }

    public Role update(Role role) {
        return em.merge(role);
    }

    public long nbRole() {
        String requete = "select count(c) from Role c";
        TypedQuery<Long> query = em.createQuery(requete, Long.class);
        return query.getSingleResult();
    }
    
    
  
}
