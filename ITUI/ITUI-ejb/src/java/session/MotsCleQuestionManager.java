/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Motsclequestion;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Nante-PC
 */
@Stateless
@LocalBean
public class MotsCleQuestionManager {
    @Resource
    private SessionContext sessionContext;
    @PersistenceContext(unitName = "ITUI-ejbPU")
    private EntityManager em;

    public void creerMotsCleQuestion(Motsclequestion motsCleQuestion) {
        em.persist(motsCleQuestion);
    }
    
    public List<Motsclequestion> getAllMotsCleQuestion() {
        String requete = "select c from Motsclequestion c";
        TypedQuery<Motsclequestion> query = em.createQuery(requete, Motsclequestion.class);
        return query.getResultList();
    }
    
    public void supprimerMotsCleQuestion(Motsclequestion motsCleQuestion) {
        em.remove(em.merge(motsCleQuestion));
    }
    
    public Motsclequestion getMotsCleQuestion(Long id) {
        return em.find(Motsclequestion.class, id);
    }

    public Motsclequestion update(Motsclequestion motsCleQuestion) {
        return em.merge(motsCleQuestion);
    }
    
    public long nbMotsCleQuestion() {
        String requete = "select count(c) from Motsclequestion c";
        TypedQuery<Long> query = em.createQuery(requete, Long.class);
        return query.getSingleResult();
    }
}
