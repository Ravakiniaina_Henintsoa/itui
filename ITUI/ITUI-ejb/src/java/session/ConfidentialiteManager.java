/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Confidentialite;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Nante-PC
 */
@Stateless
@LocalBean
public class ConfidentialiteManager {
    @Resource
    private SessionContext sessionContext;
    
    @PersistenceContext(unitName = "ITUI-ejbPU")
    private EntityManager em;

    public void creerConfidentialite(Confidentialite confidentialite) {
        em.persist(confidentialite);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    public List<Confidentialite> getAllConfidentialite() {
        String requete = "select c from Confidentialite c";
        TypedQuery<Confidentialite> query = em.createQuery(requete, Confidentialite.class);
        return query.getResultList();
    }

    public void supprimerConfidentialite(Confidentialite confidentialite) {
        em.remove(em.merge(confidentialite));
    }

    public Confidentialite getConfidentialite(long id) {
        return em.find(Confidentialite.class, id);
    }

    public Confidentialite update(Confidentialite confidentialite) {
        return em.merge(confidentialite);
    }

    public long nbConfidentialite() {
        String requete = "select count(c) from Confidentialite c";
        TypedQuery<Long> query = em.createQuery(requete, Long.class);
        return query.getSingleResult();
    }    
}
