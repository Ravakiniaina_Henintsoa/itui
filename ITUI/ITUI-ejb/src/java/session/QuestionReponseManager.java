/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Questionreponse;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Nante-PC
 */
@Stateless
@LocalBean
public class QuestionReponseManager {
    @Resource
    private SessionContext sessionContext;
    @PersistenceContext(unitName = "ITUI-ejbPU")
    private EntityManager em;

    public void creerQuestionReponse(Questionreponse questionreponse) {
        em.persist(questionreponse);
    }
    
    public List<Questionreponse> getAllQuestionreponse() {
        String requete = "select c from Questionreponse c";
        TypedQuery<Questionreponse> query = em.createQuery(requete, Questionreponse.class);
        return query.getResultList();
    }
    
    public void supprimerQuestionreponse(Questionreponse questionreponse) {
        em.remove(em.merge(questionreponse));
    }
    
    public Questionreponse getQuestionreponse(Long id) {
        return em.find(Questionreponse.class, id);
    }

    public Questionreponse update(Questionreponse questionreponse) {
        return em.merge(questionreponse);
    }
    
    public long nbQuestionreponse() {
        String requete = "select count(c) from Questionreponse c";
        TypedQuery<Long> query = em.createQuery(requete, Long.class);
        return query.getSingleResult();
    }
}
