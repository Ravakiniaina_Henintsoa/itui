/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Reponse;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Nante-PC
 */
@Stateless
@LocalBean
public class ReponseManager {
    @Resource
    private SessionContext sessionContext;
      
    @PersistenceContext(unitName = "ITUI-ejbPU")
    private EntityManager em;

    public void creerReponse(Reponse reponse) {
        em.persist(reponse);
    }

    public List<Reponse> getAllReponse() {
        String requete = "select c from Reponse c";
        TypedQuery<Reponse> query = em.createQuery(requete, Reponse.class);
        return query.getResultList();
    }

    public void supprimerReponse(Reponse reponse) {
        em.remove(em.merge(reponse));
    }

    public Reponse getReponse(long id) {
        return em.find(Reponse.class, id);
    }

    public Reponse update(Reponse reponse) {
        return em.merge(reponse);
    }

    public long nbReponse() {
        String requete = "select count(c) from Reponse c";
        TypedQuery<Long> query = em.createQuery(requete, Long.class);
        return query.getSingleResult();
    }
}
