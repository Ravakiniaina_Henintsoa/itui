/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Notification;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Nante-PC
 */
@Stateless
@LocalBean
public class NotificationManager {
    @Resource
    private SessionContext sessionContext;
    @PersistenceContext(unitName = "ITUI-ejbPU")
    private EntityManager em;

    public void creerNotification(Notification notification) {
        em.persist(notification);
    }
    
    public List<Notification> getAllNotification() {
        String requete = "select c from Notification c";
        TypedQuery<Notification> query = em.createQuery(requete, Notification.class);
        return query.getResultList();
    }
    
    public void supprimerNotification(Notification notification) {
        em.remove(em.merge(notification));
    }
    
    public Notification getNotification(Long id) {
        return em.find(Notification.class, id);
    }

    public Notification update(Notification notification) {
        return em.merge(notification);
    }
    
    public long nbNotification() {
        String requete = "select count(c) from Notification c";
        TypedQuery<Long> query = em.createQuery(requete, Long.class);
        return query.getSingleResult();
    }
}
