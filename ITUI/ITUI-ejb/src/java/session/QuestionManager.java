/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Question;
import entity.Utilisateur;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Nante-PC
 */
@Stateless
@LocalBean
public class QuestionManager {
    @Resource
    private SessionContext sessionContext;
    @PersistenceContext(unitName = "ITUI-ejbPU")
    private EntityManager em;

    public void creerQuestion(Question question) {
        em.persist(question);
    }
    
    public List<Question> getAllQuestion() {
        String requete = "select c from Question c where c.etat = 0";
        TypedQuery<Question> query = em.createQuery(requete, Question.class);
        return query.getResultList();
    }
    
    public List<Question> findByUser(Long id) {
        TypedQuery<Question> query = em.createNamedQuery("Question.findById", Question.class);
        query.setParameter("id", id);
        return query.getResultList();
    }
    
    public void supprimerQuestion(Question question) {
        em.remove(em.merge(question));
    }
    
    public Question getQuestion(Long id) {
        return em.find(Question.class, id);
    }

    public Question update(Question question) {
        return em.merge(question);
    }
    
    public long nbQuestion() {
        String requete = "select count(c) from Question c";
        TypedQuery<Long> query = em.createQuery(requete, Long.class);
        return query.getSingleResult();
    }
}
