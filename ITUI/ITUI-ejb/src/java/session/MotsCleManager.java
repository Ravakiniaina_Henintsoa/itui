/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Motscle;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Nante-PC
 */
@Stateless
@LocalBean
public class MotsCleManager {

    @PersistenceContext(unitName = "ITUI-ejbPU")
    private EntityManager em;
    @Resource
    private SessionContext sessionContext;

    
    public List<Motscle> getAllMotsCle() {
        String requete = "select c from Motscle c";
        TypedQuery<Motscle> query = em.createQuery(requete, Motscle.class);
        return query.getResultList();
    }
    
    public void supprimerMotsCle(Motscle motsCle) {
        em.remove(em.merge(motsCle));
    }
    
    public Motscle getMotsCle(Long id) {
        return em.find(Motscle.class, id);
    }

    public Motscle update(Motscle motsCle) {
        return em.merge(motsCle);
    }
    
    public Long nbMotsCle() {
        String requete = "select count(c) from Motscle c";
        TypedQuery<Long> query = em.createQuery(requete, Long.class);
        return query.getSingleResult();
    }

    public void creerMotsCle(Motscle motscle) {
        em.persist(motscle);
    }
}
