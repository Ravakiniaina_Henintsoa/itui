/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Questionreponse;
import entity.Roleutilisateur;
import java.util.List;
import javax.annotation.Resource;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author pro
 */

@Stateless
@LocalBean

public class UtilisateurRoleManager {
    @Resource
    private SessionContext sessionContext;
    @PersistenceContext(unitName = "ITUI-ejbPU")
    private EntityManager em;
  
    public void creerUtilisateurRole(Roleutilisateur utilisateurRole) {
        em.persist(utilisateurRole);
    }
    
    public List<Roleutilisateur> getAllUtilisateurRole() {
        String requete = "select c from Roleutilisateur c";
        TypedQuery<Roleutilisateur> query = em.createQuery(requete, Roleutilisateur.class);
        return query.getResultList();
    }
    
    public void supprimerUtilisateurRole(Roleutilisateur utilisateurRole) {
        em.remove(em.merge(utilisateurRole));
    }
    
    public Roleutilisateur getUtilisateurRole(Long id) {
        return em.find(Roleutilisateur.class, id);
    }

    public Roleutilisateur update(Roleutilisateur utilisateurRole) {
        return em.merge(utilisateurRole);
    }
    
    public long nbUtilisateurRole() {
        String requete = "select count(c) from Roleutilisateur c";
        TypedQuery<Long> query = em.createQuery(requete, Long.class);
        return query.getSingleResult();
    }
}
