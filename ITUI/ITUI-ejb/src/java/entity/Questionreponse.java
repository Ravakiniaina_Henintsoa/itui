/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nante-PC
 */
@Entity
@Table(name = "QUESTIONREPONSE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Questionreponse.findAll", query = "SELECT q FROM Questionreponse q")
    , @NamedQuery(name = "Questionreponse.findById", query = "SELECT q FROM Questionreponse q WHERE q.id = :id")})
public class Questionreponse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "QUESTION_ID", referencedColumnName = "ID")
    @ManyToOne
    private Question questionId;
    @JoinColumn(name = "REPONSE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Reponse reponseId;

    public Questionreponse() {
    }

    public Questionreponse(Long id) {
        this.id = id;
    }
    public Questionreponse(Question question, Reponse reponse) {
        this.questionId = question;
        this.reponseId = reponse;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Question getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Question questionId) {
        this.questionId = questionId;
    }

    public Reponse getReponseId() {
        return reponseId;
    }

    public void setReponseId(Reponse reponseId) {
        this.reponseId = reponseId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Questionreponse)) {
            return false;
        }
        Questionreponse other = (Questionreponse) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Questionreponse[ id=" + id + " ]";
    }
    
}
