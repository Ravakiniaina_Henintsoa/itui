/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Nante-PC
 */
@Entity
@Table(name = "REPONSE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reponse.findAll", query = "SELECT r FROM Reponse r")
    , @NamedQuery(name = "Reponse.findById", query = "SELECT r FROM Reponse r WHERE r.id = :id")
    , @NamedQuery(name = "Reponse.findByDaty", query = "SELECT r FROM Reponse r WHERE r.daty = :daty")
    , @NamedQuery(name = "Reponse.findByEtat", query = "SELECT r FROM Reponse r WHERE r.etat = :etat")
    , @NamedQuery(name = "Reponse.findByHeure", query = "SELECT r FROM Reponse r WHERE r.heure = :heure")
    , @NamedQuery(name = "Reponse.findByLibelle", query = "SELECT r FROM Reponse r WHERE r.libelle = :libelle")
    , @NamedQuery(name = "Reponse.findByVote", query = "SELECT r FROM Reponse r WHERE r.vote = :vote")})
public class Reponse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "DATY")
    @Temporal(TemporalType.DATE)
    private Date daty;
    @Column(name = "ETAT")
    private Integer etat;
    @Column(name = "HEURE")
    private String heure;
    @Column(name = "LIBELLE")
    private String libelle;
    @Column(name = "VOTE")
    private Integer vote;
    @OneToMany(mappedBy = "reponseId")
    private Collection<Questionreponse> questionreponseCollection;
    @JoinColumn(name = "CONFIDENTIALITE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Confidentialite confidentialiteId;
    @JoinColumn(name = "QUESTION_ID", referencedColumnName = "ID")
    @ManyToOne
    private Question questionId;
    @JoinColumn(name = "UTILISATEUR_ID", referencedColumnName = "ID")
    @ManyToOne
    private Utilisateur utilisateurId;

    public Reponse() {
    }

    public Reponse(Utilisateur utilisateur, String libelle, Confidentialite confidentialite, Date daty, String heure, int vote, int etat, Question question) {
        this.utilisateurId = utilisateur;
        this.libelle = libelle;
        this.confidentialiteId = confidentialite;
        this.daty = daty;
        this.heure = heure;
        this.vote = vote;
        this.etat = etat;
        this.questionId = question;
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getVote() {
        return vote;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }

    @XmlTransient
    public Collection<Questionreponse> getQuestionreponseCollection() {
        return questionreponseCollection;
    }

    public void setQuestionreponseCollection(Collection<Questionreponse> questionreponseCollection) {
        this.questionreponseCollection = questionreponseCollection;
    }

    public Confidentialite getConfidentialiteId() {
        return confidentialiteId;
    }

    public void setConfidentialiteId(Confidentialite confidentialiteId) {
        this.confidentialiteId = confidentialiteId;
    }

    public Question getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Question questionId) {
        this.questionId = questionId;
    }

    public Utilisateur getUtilisateurId() {
        return utilisateurId;
    }

    public void setUtilisateurId(Utilisateur utilisateurId) {
        this.utilisateurId = utilisateurId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reponse)) {
            return false;
        }
        Reponse other = (Reponse) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Reponse[ id=" + id + " ]";
    }
    
}
