/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Nante-PC
 */
@Entity
@Table(name = "MOTSCLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Motscle.findAll", query = "SELECT m FROM Motscle m")
    , @NamedQuery(name = "Motscle.findById", query = "SELECT m FROM Motscle m WHERE m.id = :id")
    , @NamedQuery(name = "Motscle.findByContenu", query = "SELECT m FROM Motscle m WHERE m.contenu = :contenu")
    , @NamedQuery(name = "Motscle.findByEtat", query = "SELECT m FROM Motscle m WHERE m.etat = :etat")})
public class Motscle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "CONTENU")
    private String contenu;
    @Column(name = "ETAT")
    private int etat;
    @JoinTable(name = "QUESTION_MOTSCLE", joinColumns = {
        @JoinColumn(name = "MOTSCLES_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "QUESTION_ID", referencedColumnName = "ID")})
    @ManyToMany
    private Collection<Question> questionCollection;
    @OneToMany(mappedBy = "motscleId")
    private Collection<Motsclequestion> motsclequestionCollection;

    public Motscle() {
    }

    public Motscle(String contenu, int etat) {
        this.contenu = contenu;
        this.etat = etat;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @XmlTransient
    public Collection<Question> getQuestionCollection() {
        return questionCollection;
    }

    public void setQuestionCollection(Collection<Question> questionCollection) {
        this.questionCollection = questionCollection;
    }

    @XmlTransient
    public Collection<Motsclequestion> getMotsclequestionCollection() {
        return motsclequestionCollection;
    }

    public void setMotsclequestionCollection(Collection<Motsclequestion> motsclequestionCollection) {
        this.motsclequestionCollection = motsclequestionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Motscle)) {
            return false;
        }
        Motscle other = (Motscle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Motscle[ id=" + id + " ]";
    }
    
}
