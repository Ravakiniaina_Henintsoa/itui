/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Nante-PC
 */
@Entity
@Table(name = "CONFIDENTIALITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Confidentialite.findAll", query = "SELECT c FROM Confidentialite c")
    , @NamedQuery(name = "Confidentialite.findById", query = "SELECT c FROM Confidentialite c WHERE c.id = :id")
    , @NamedQuery(name = "Confidentialite.findByLibelle", query = "SELECT c FROM Confidentialite c WHERE c.libelle = :libelle")})
public class Confidentialite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "LIBELLE")
    private String libelle;
    @OneToMany(mappedBy = "confidentialiteId")
    private Collection<Reponse> reponseCollection;
    @OneToMany(mappedBy = "confidentialiteId")
    private Collection<Question> questionCollection;

    public Confidentialite() {
    }

    public Confidentialite(Long id) {
        this.id = id;
    }
    public Confidentialite(String libelle) {
        this.libelle = libelle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @XmlTransient
    public Collection<Reponse> getReponseCollection() {
        return reponseCollection;
    }

    public void setReponseCollection(Collection<Reponse> reponseCollection) {
        this.reponseCollection = reponseCollection;
    }

    @XmlTransient
    public Collection<Question> getQuestionCollection() {
        return questionCollection;
    }

    public void setQuestionCollection(Collection<Question> questionCollection) {
        this.questionCollection = questionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Confidentialite)) {
            return false;
        }
        Confidentialite other = (Confidentialite) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Confidentialite[ id=" + id + " ]";
    }
    
}
