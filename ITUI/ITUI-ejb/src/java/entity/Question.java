/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Nante-PC
 */
@Entity
@Table(name = "QUESTION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Question.findAll", query = "SELECT q FROM Question q")
    , @NamedQuery(name = "Question.findById", query = "SELECT q FROM Question q WHERE q.id = :id")
    , @NamedQuery(name = "Question.findByCategorie", query = "SELECT q FROM Question q WHERE q.categorie = :categorie")
    , @NamedQuery(name = "Question.findByDaty", query = "SELECT q FROM Question q WHERE q.daty = :daty")
    , @NamedQuery(name = "Question.findByEtat", query = "SELECT q FROM Question q WHERE q.etat = :etat")
    , @NamedQuery(name = "Question.findByHeure", query = "SELECT q FROM Question q WHERE q.heure = :heure")
    , @NamedQuery(name = "Question.findByLibelle", query = "SELECT q FROM Question q WHERE q.libelle = :libelle")
})
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "CATEGORIE")
    private String categorie;
    @Column(name = "DATY")
    @Temporal(TemporalType.DATE)
    private Date daty;
    @Column(name = "ETAT")
    private Integer etat;
    @Column(name = "HEURE")
    private String heure;
    @Column(name = "LIBELLE")
    private String libelle;
    @ManyToMany(mappedBy = "questionCollection")
    private Collection<Motscle> motscleCollection;
    @OneToMany(mappedBy = "questionId")
    private Collection<Questionreponse> questionreponseCollection;
    @OneToMany(mappedBy = "questionId")
    private Collection<Notification> notificationCollection;
    @OneToMany(mappedBy = "questionId")
    private Collection<Reponse> reponseCollection;
    @OneToMany(mappedBy = "questionId")
    private Collection<Motsclequestion> motsclequestionCollection;
    @JoinColumn(name = "CONFIDENTIALITE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Confidentialite confidentialiteId;
    @JoinColumn(name = "UTILISATEUR_ID", referencedColumnName = "ID")
    @ManyToOne
    private Utilisateur utilisateurId;
    @Column(name = "TITRE")
    private String titre;
    @Column(name = "CODE")
    private String code;
    
    public Question() {
    }

    public Question(Long id) {
        this.id = id;
    }
     public Question(Utilisateur utilisateur, String libelle, String categorie, Date daty, String heure, Confidentialite confidentialite, int etat, String titre, String code) {
        this.utilisateurId = utilisateur;
        this.libelle = libelle;
        this.categorie = categorie;
        this.daty = daty;
        this.heure = heure;
        this.confidentialiteId = confidentialite;
        this.etat = etat;
        this.titre = titre;
        this.code = code;
    }

    public Question(Utilisateur utilisateur, String libelle, String categorie) {
        this.utilisateurId = utilisateur;
        this.libelle = libelle;
        this.categorie = categorie;
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public Date getDaty() {
        return daty;
    }

    public void setDaty(Date daty) {
        this.daty = daty;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @XmlTransient
    public Collection<Motscle> getMotscleCollection() {
        return motscleCollection;
    }

    public void setMotscleCollection(Collection<Motscle> motscleCollection) {
        this.motscleCollection = motscleCollection;
    }

    @XmlTransient
    public Collection<Questionreponse> getQuestionreponseCollection() {
        return questionreponseCollection;
    }

    public void setQuestionreponseCollection(Collection<Questionreponse> questionreponseCollection) {
        this.questionreponseCollection = questionreponseCollection;
    }

    @XmlTransient
    public Collection<Notification> getNotificationCollection() {
        return notificationCollection;
    }

    public void setNotificationCollection(Collection<Notification> notificationCollection) {
        this.notificationCollection = notificationCollection;
    }

    @XmlTransient
    public Collection<Reponse> getReponseCollection() {
        return reponseCollection;
    }

    public void setReponseCollection(Collection<Reponse> reponseCollection) {
        this.reponseCollection = reponseCollection;
    }

    @XmlTransient
    public Collection<Motsclequestion> getMotsclequestionCollection() {
        return motsclequestionCollection;
    }

    public void setMotsclequestionCollection(Collection<Motsclequestion> motsclequestionCollection) {
        this.motsclequestionCollection = motsclequestionCollection;
    }

    public Confidentialite getConfidentialiteId() {
        return confidentialiteId;
    }

    public void setConfidentialiteId(Confidentialite confidentialiteId) {
        this.confidentialiteId = confidentialiteId;
    }

    public Utilisateur getUtilisateurId() {
        return utilisateurId;
    }

    public void setUtilisateurId(Utilisateur utilisateurId) {
        this.utilisateurId = utilisateurId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Question)) {
            return false;
        }
        Question other = (Question) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Question[ id=" + id + " ]";
    }

    /**
     * @return the titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     * @param titre the titre to set
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }
    
}
