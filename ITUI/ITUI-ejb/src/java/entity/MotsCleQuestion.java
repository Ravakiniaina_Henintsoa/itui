/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Nante-PC
 */
@Entity
@Table(name = "MOTSCLEQUESTION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Motsclequestion.findAll", query = "SELECT m FROM Motsclequestion m")
    , @NamedQuery(name = "Motsclequestion.findById", query = "SELECT m FROM Motsclequestion m WHERE m.id = :id")})
public class Motsclequestion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "MOTSCLE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Motscle motscleId;
    @JoinColumn(name = "QUESTION_ID", referencedColumnName = "ID")
    @ManyToOne
    private Question questionId;

    public Motsclequestion() {
    }

    public Motsclequestion(Long id) {
        this.id = id;
    }
    public Motsclequestion(Question question, Motscle motscle) {
        this.questionId = question;
        this.motscleId = motscle;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Motscle getMotscleId() {
        return motscleId;
    }

    public void setMotscleId(Motscle motscleId) {
        this.motscleId = motscleId;
    }

    public Question getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Question questionId) {
        this.questionId = questionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Motsclequestion)) {
            return false;
        }
        Motsclequestion other = (Motsclequestion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Motsclequestion[ id=" + id + " ]";
    }
    
}
